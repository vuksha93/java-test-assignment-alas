package com.alasdoo.developercourseassignment;

import com.alasdoo.developercourseassignment.pages.CoursePage;
import com.alasdoo.developercourseassignment.pages.StudentPage;
import com.alasdoo.developercourseassignment.pages.TeacherPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageObject {
    protected WebDriver driver;

    public PageObject(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "a[href='/student']")
    protected WebElement studentsButton;

    @FindBy(css = "a[href='/teacher']")
    protected WebElement teachersButton;

    @FindBy(css = "a[href='/course']")
    protected WebElement coursesButton;

    @FindBy(css = "a[href='/settings']")
    protected WebElement settingsButton;

    @FindBy(css = "[aria-label=add]")
    protected WebElement addButton;

    @FindBy(css = "button[data-test-id=save]")
    protected WebElement saveButton;

    @FindBy(css = "button[data-test-id=delete]")
    protected WebElement deleteButton;

    @FindBy(css = "button[title='Next page']")
    protected WebElement nextPageButton;

    @FindBy(css = "button[title='Previous page']")
    protected WebElement previousPageButton;

    // For some reason this selector could not be found
    @FindBy(className = "MuiTypography-root MuiTablePagination-caption makeStyles-caption-9 MuiTypography-body2 MuiTypography-colorInherit")
    protected WebElement numberOfRecords;

    public int getNumberOfRecords() {
        String text = this.numberOfRecords.getText();
        String numberOfRecords = text.substring(text.lastIndexOf(" ") + 1);
        return Integer.parseInt(numberOfRecords);
    }

    public StudentPage goToStudentPage() {
        studentsButton.click();
        return new StudentPage(driver);
    }

    public TeacherPage goToTeacherPage() {
        teachersButton.click();
        return new TeacherPage(driver);
    }

    public CoursePage goToCoursePage() {
        coursesButton.click();
        return new CoursePage(driver);
    }

}
