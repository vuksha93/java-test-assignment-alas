package com.alasdoo.developercourseassignment.pages;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddCoursePage extends CoursePage {

    @FindBy(css = "input[name=developerCourseName]")
    protected WebElement inputName;

    @FindBy(css = "input[name=costPerClass]")
    protected WebElement inputCostPerClass;

    @FindBy(css = "input[name=classesPerWeek]")
    protected WebElement inputClassesPerWeek;

    public AddCoursePage(WebDriver driver) {
        super(driver);
        assertTrue(inputClassesPerWeek.isDisplayed());
    }

    public void addNewCourse(String name, String costPerClass, String classesPerWeek) {
        this.inputName.clear();
        this.inputName.sendKeys(name);
        this.inputCostPerClass.clear();
        this.inputCostPerClass.sendKeys(costPerClass);
        this.inputClassesPerWeek.clear();
        this.inputClassesPerWeek.sendKeys(classesPerWeek);
    }

    public UpdateCoursePage submit() {
        saveButton.click();
        return new UpdateCoursePage(driver);
    }

}
