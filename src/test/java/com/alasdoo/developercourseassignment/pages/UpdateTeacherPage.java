package com.alasdoo.developercourseassignment.pages;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class UpdateTeacherPage extends AddTeacherPage {

    @FindBy(css = "button[data-test-id=courses]")
    private WebElement toggleButton;

    public UpdateTeacherPage(WebDriver driver) {
        super(driver);
        assertTrue(toggleButton.isDisplayed());
    }

}
