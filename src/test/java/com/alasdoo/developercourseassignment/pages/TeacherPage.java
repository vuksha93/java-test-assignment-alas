package com.alasdoo.developercourseassignment.pages;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.alasdoo.developercourseassignment.PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TeacherPage extends PageObject {

    @FindBy(css = "[data-field=teacherName]")
    private WebElement name;

    public TeacherPage(WebDriver driver) {
        super(driver);
        assertTrue(name.isDisplayed());
    }

    public AddTeacherPage goToAddTeacherPage() {
        addButton.click();
        return new AddTeacherPage(driver);
    }

}
