package com.alasdoo.developercourseassignment.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddTeacherPage extends TeacherPage {

    @FindBy(css = "input[name=teacherName]")
    protected WebElement inputName;

    @FindBy(css = "input[name=teacherSurname]")
    protected WebElement inputSurname;

    @FindBy(css = "input[name=teacherEmail]")
    protected WebElement inputEmail;

    public AddTeacherPage(WebDriver driver) {
        super(driver);
    }

    public void addNewTeacher(String name, String surname, String email) {
        this.inputName.clear();
        this.inputName.sendKeys(name);
        this.inputSurname.clear();
        this.inputSurname.sendKeys(surname);
        this.inputEmail.clear();
        this.inputEmail.sendKeys(email);
    }

    public UpdateTeacherPage submit() {
        saveButton.click();
        return new UpdateTeacherPage(driver);
    }

}
