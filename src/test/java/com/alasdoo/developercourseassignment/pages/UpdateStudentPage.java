package com.alasdoo.developercourseassignment.pages;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class UpdateStudentPage extends AddStudentPage {

    @FindBy(css = "input[name=accountName][disabled]")
    private WebElement disabledInputAccountName;

    public UpdateStudentPage(WebDriver driver) {
        super(driver);
        assertTrue(disabledInputAccountName.isDisplayed());
    }

}
