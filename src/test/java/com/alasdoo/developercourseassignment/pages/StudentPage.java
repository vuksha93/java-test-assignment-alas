package com.alasdoo.developercourseassignment.pages;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.alasdoo.developercourseassignment.PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class StudentPage extends PageObject {

    @FindBy(css = "[data-field=accountName]")
    private WebElement accountName;

    public StudentPage(WebDriver driver) {
        super(driver);
        assertTrue(accountName.isDisplayed());
    }

    public AddStudentPage goToAddStudentPage() {
        addButton.click();
        return new AddStudentPage(driver);
    }

}
