package com.alasdoo.developercourseassignment.pages;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.alasdoo.developercourseassignment.PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CoursePage extends PageObject {

    @FindBy(css = "[data-field=developerCourseName]")
    private WebElement courseName;

    public CoursePage(WebDriver driver) {
        super(driver);
        assertTrue(courseName.isDisplayed());
    }

    public AddCoursePage goToCoursePage() {
        addButton.click();
        return new AddCoursePage(driver);
    }

}
