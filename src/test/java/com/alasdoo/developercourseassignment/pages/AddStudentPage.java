package com.alasdoo.developercourseassignment.pages;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddStudentPage extends StudentPage {

    @FindBy(css = "input[name=name]")
    protected WebElement inputName;

    @FindBy(css = "input[name=surname]")
    protected WebElement inputSurname;

    @FindBy(css = "input[name=accountName]")
    protected WebElement inputAccountName;

    @FindBy(css = "input[name=email]")
    protected WebElement inputEmail;

    @FindBy(css = "input[name=bankCardNumber]")
    protected WebElement inputBankCardNumber;

    public AddStudentPage(WebDriver driver) {
        super(driver);
        assertTrue(inputAccountName.isDisplayed());
    }

    public void addNewStudent(String name, String surname, String accountName, String email, String bankCardNumber) {
        this.inputName.clear();
        this.inputName.sendKeys(name);
        this.inputSurname.clear();
        this.inputSurname.sendKeys(surname);
        this.inputAccountName.clear();
        this.inputAccountName.sendKeys(accountName);
        this.inputEmail.clear();
        this.inputEmail.sendKeys(email);
        this.inputBankCardNumber.clear();
        this.inputBankCardNumber.sendKeys(bankCardNumber);
    }

    public UpdateStudentPage submit() {
        saveButton.click();
        return new UpdateStudentPage(driver);
    }

}
