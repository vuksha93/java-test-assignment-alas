package com.alasdoo.developercourseassignment.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.alasdoo.developercourseassignment.FunctionalTest;
import com.alasdoo.developercourseassignment.pages.AddCoursePage;
import com.alasdoo.developercourseassignment.pages.UpdateCoursePage;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AddCoursePageTest extends FunctionalTest {

    @BeforeEach
    public void getOnPage() {
        driver.get("http://localhost:3000/course/new");
    }

    @Test
    public void addNewCourse() {
        AddCoursePage addCoursePage = new AddCoursePage(driver);
        String name = "New course";
        String costPerClass = "50";
        String classesPerWeek = "3";

        int numberOfRecordsBefore = addCoursePage.getNumberOfRecords();
        addCoursePage.addNewCourse(name, costPerClass, classesPerWeek);
        UpdateCoursePage updateCoursePage = addCoursePage.submit();
        int numberOfRecordsAfter = addCoursePage.getNumberOfRecords();

        assertEquals(numberOfRecordsBefore + 1, numberOfRecordsAfter);
        assertEquals(name, updateCoursePage.getInputName().getAttribute("value"));
        assertEquals(costPerClass, updateCoursePage.getInputCostPerClass().getAttribute("value"));
        assertEquals(classesPerWeek, updateCoursePage.getInputClassesPerWeek().getAttribute("value"));

    }

}
