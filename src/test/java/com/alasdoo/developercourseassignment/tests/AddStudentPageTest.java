package com.alasdoo.developercourseassignment.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import com.alasdoo.developercourseassignment.FunctionalTest;
import com.alasdoo.developercourseassignment.pages.AddStudentPage;
import com.alasdoo.developercourseassignment.pages.UpdateStudentPage;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AddStudentPageTest extends FunctionalTest {

    @BeforeEach
    public void getOnPage() {
        driver.get("http://localhost:3000/student/new");
    }

    @Test
    public void addNewStudent() {
        Date date = new Date();
        AddStudentPage addStudentPage = new AddStudentPage(driver);
        String name = "Nemanja";
        String surname = "Vukojevic";
        String accountName = "vuksha" + date.getTime();
        String email = "vuksha" + date.getTime() + "@gmail.com";
        String bankCardNumber = "111";

        addStudentPage.addNewStudent(name, surname, accountName, email, bankCardNumber);
        UpdateStudentPage updateStudentPage = addStudentPage.submit();

        assertEquals(name, updateStudentPage.getInputName().getAttribute("value"));
        assertEquals(surname, updateStudentPage.getInputSurname().getAttribute("value"));
        assertEquals(accountName, updateStudentPage.getInputAccountName().getAttribute("value"));
        assertEquals(email, updateStudentPage.getInputEmail().getAttribute("value"));
        assertEquals(bankCardNumber, updateStudentPage.getInputBankCardNumber().getAttribute("value"));
    }

}
