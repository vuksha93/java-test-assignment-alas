package com.alasdoo.developercourseassignment.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import com.alasdoo.developercourseassignment.FunctionalTest;
import com.alasdoo.developercourseassignment.pages.AddTeacherPage;
import com.alasdoo.developercourseassignment.pages.UpdateTeacherPage;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AddTeacherPageTest extends FunctionalTest {

    @BeforeEach
    public void getOnPage() {
        driver.get("http://localhost:3000/teacher/new");
    }

    @Test
    public void addNewTeacher() {
        Date date = new Date();
        AddTeacherPage addTeacherPage = new AddTeacherPage(driver);
        String name = "Pera";
        String surname = "Peric";
        String email = "pera" + date.getTime() + "@gmail.com";

        addTeacherPage.addNewTeacher(name, surname, email);
        UpdateTeacherPage updateTeacherPage = addTeacherPage.submit();

        assertEquals(name, updateTeacherPage.getInputName().getAttribute("value"));
        assertEquals(surname, updateTeacherPage.getInputSurname().getAttribute("value"));
        assertEquals(email, updateTeacherPage.getInputEmail().getAttribute("value"));

    }

}
