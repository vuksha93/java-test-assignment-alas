package com.alasdoo.developercourseassignment.repository;

import java.util.Optional;

import com.alasdoo.developercourseassignment.entity.Teacher;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Integer> {

    Optional<Teacher> findByTeacherNameAndTeacherSurname(String name, String surname);

    Optional<Teacher> findByTeacherEmail(String email);

}
